# Crypto

<!-- vim-markdown-toc GitLab -->

* [I. Hashing](#i-hashing)
* [II. Chiffrement](#ii-chiffrement)
    * [1. Chiffrement symétrique](#1-chiffrement-symétrique)
    * [2. Chiffrement asymétrique](#2-chiffrement-asymétrique)
        * [A. Signature](#a-signature)
        * [B. Chiffrement](#b-chiffrement)
        * [C. Cas réel](#c-cas-réel)
* [III. HTTPS et TLS](#iii-https-et-tls)
    * [1. Echange de clés](#1-echange-de-clés)
    * [2. Certificat](#2-certificat)

<!-- vim-markdown-toc -->

# I. Hashing

Le *hashing* est le fait d'utiliser une fonction mathématique, appelée fonction de hachage, afin de calculer *l'empreinte* ou *hash* d'un fichier ou d'une chaîne de caractère.  
Cette action est par définition **irréversible**.

Ces fonctions de hachage possèdent plusieurs propriétés :
* **fonctions à sens unique**
  * on ne peut pas, à partir d'un *hash*, remonter à la donnée entrée dans la fonction de hachage
* **non prédictibles**
  * un petit changement dans la donnée d'entrée résulte en un changement total du *hash*
  * difficile de trouver des *collisions* : deux entrées différentes qui donneraient le même *hash*
* aussi, un hash doit être **facile et rapide à calculer**

Exemples de fonctions de hachage :
* [MD5](https://en.wikipedia.org/wiki/MD5) (obsolète)
* [SHA-1](https://en.wikipedia.org/wiki/SHA-1)
* [SHA-2](https://en.wikipedia.org/wiki/SHA-2)

Il est possible de calculer rapidement un *hash* depuis n'importe quel langage, y compris `bash` :
```bash
$ echo 'test' | md5sum
$ echo 'test' | sha1sum
$ echo 'test' | sha256sum
```

# II. Chiffrement

Le chiffrement est le fait d'utiliser un **algorithme de chiffrement** ainsi qu'une **clé de chiffrement** afin de transformer un fichier ou chaîne de caractère en un message "codé" ou plutôt, **chiffré**. Un message chiffré est illisible, et ne permet pas retrouver le message original, sauf pour les détenteurs de la **clé de déchiffrement**.

> ["Chiffrer" et pas "crypter" :D](https://chiffrer.info/).

Il existe deux types de chiffrements :
* **symétrique**
  * la clé de chiffrement et la clé de déchiffrement sont les mêmes
* **asymétrique**
  * la clé de chiffrement et de déchiffrement sont différentes

## 1. Chiffrement symétrique

Les chiffrements symétriques sont des chiffrements simples et **performants**. On peut citer :
* [chiffrement par décalage](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage)
  * simple décalage de lettres dans l'alphabet
  * la clé est le nombre de lettres dont on décale dans l'alphabet
* d'autres chiffrements bien plus robustes qu'un chiffrement par décalage existent, comme [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard)
  * il existe plusieurs variantes d'AES
  * AES est utilisé dans la plupart des communications chiffrées nécessitant performances et sécurité (comme les connexions HTTPS)

> L'inconvénient d'une telle méthode est qu'elle nécessite l'échange préalable d'une clé, **avant** l'établissement d'un tunnel de communication chiffré.

## 2. Chiffrement asymétrique

Le chiffrement asymétrique repose sur des algorithmes qui ont la particularité d'utiliser des clés différentes pour le chiffrement et le déchiffrement des données. 

Ainsi, lors de la génération initiale de clés pour pouvoir mettre en place le chiffrement, deux clés sont générées. Tout ce que chiffre l'une d'entre elles, l'autre peut le déchiffrer.

Il est possible de mettre en place deux choses avec un chiffrement asymétrique :
* **signature**
  * garantir l'authenticité et l'identité d'un participant lors d'un échange
* **chiffrement**
  * garantir la confidentialité d'un message lors de son transit

On peut citer par exemple [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem) ou encore [EC](https://en.wikipedia.org/wiki/Elliptic-curve_cryptography).

### A. Signature

Supposons deux participants Alice et Bob.

Alice génère sa paire de clés. L'une est dite *publique* (peu importe laquelle), l'autre est alors dite *privée*.

Alice garde sa clé *privée* `cléPrivA` et ne la partagera jamais avec personne.  
Alice distribue sa clé *publique* `cléPubA`, y compris à Bob. 

Alice peut alors envoyer un message à Bob en le chiffrant avec sa clé privée `cléPrivA`.  
Si Bob peut peut déchiffrer le message, **alors il a la garantie que c'est bien Alice qui l'a envoyé : seule la clé privée `cléPrivA` d'Alice a pu chiffrer ce message**.

**Alice a donc *signé* son message : elle a prouvé son *identité* à Bob.**

### B. Chiffrement

Supposons deux participants Alice et Bob.

Alice génère sa paire de clés. L'une est dite *publique* (peu importe laquelle), l'autre est alors dite *privée*.

Alice garde sa clé *privée* `cléPrivA` et ne la partagera jamais avec personne.  
Alice distribue sa clé *publique* `cléPubA`, y compris à Bob. 

Bob peut alors chiffrer un message avec la clé publique `cléPubA` d'Alice, et l'envoyer à Alice.  
**Seule Alice, détenteuse de la clé privée `cléPrivA`**, pourra déchiffrer le message.

**Bob a donc garanti la *confidentialité* du message qu'il voulait envoyer à Alice.**

### C. Cas réel

Si on récapitule les deux exemples ci-dessus concernant la signature et le chiffrement avec un algo asymétrique :
1. Alice génère une paire de clés
2. elle garde la privée, donne la publique à tout le monde, y compris Bob
3. Alice peut alors signer ses messages
4. tout le monde, y compris Bob, peut lui envoyer des messages confidentiels

Dans un cas réel, Bob fait de même de son côté, et donc distribue sa clé publique à tout le monde. 

**Ainsi, chacune des deux parties, Alice et Bob, peuvent à la fois signer leurs messages et s'échanger des messages confidentiels.**

# III. HTTPS et TLS

HTTPS est le fait de mettre en place le protocole TLS afin de sécuriser des sites web HTTP.

TLS est un protocole de chiffrement décrivant l'échange entre un client et un serveur afin que :
* le client puisse s'assurer de l'identité du serveur
* le serveur et le client puisse s'échanger des données de façon confidentielle

Ceci repose sur du chiffrement asymétrique pour la vérification de l'identité, et du chiffrement à la fois symétrique ET asymétrique pour la confidentialité.

## 1. Echange de clés

Le serveur génère sa paire de clés une fois, et l'utilise pour tous ses clients.  
Les clients génèrent eux aussi une clé lors de la visite d'un site web.  
Chaque partie envoie sa clé publique à l'autre partie.

Le serveur va alors envoyer de façon confidentielle une nouvelle clé au client, qui permettra d'utiliser un chiffrement symétrique, comme AES.  
En effet, pour des raisons de performances, il est de loin préférable d'utiliser des chiffrements symétriques comme AES, plutôt qu'asymétriques.  

**Cela va permettre la confidentialité des échanges.**

## 2. Certificat

Le certificat est fourni par le serveur au client et a pour but principal de transporter la clé publique du serveur. 

Les autres informations du certificat sont des métadonnées concernant la clé ou le certificat :
* date d'expiration
* nom de domaines pour lequel il est valide
* etc.

L'élément qui va permettre de garantir **l'identité** du serveur au client est la signature d'une *Autorité de Certification* ou *CA (Certificate Authority)* connue. 

Par défaut, dès l'installation, nos OS et nos navigateurs Web possèdent un *magasin de certificats* déjà remplis. C'est une liste de certificats (donc de clé publique) d'acteurs reconnus.  
**La présence de leurs certificats dans nos magasins de certificats impliquent une confiance totale à tout ce qu'une des CA signe.**  

Pour garantir de son identité, l'administrateur d'un site web doit donc demander à une CA connue de signer le certificat de son site Web. 

**Ainsi, en plus de la confidentialité des échanges, l'administrateur pourra alors aussi garantir l'identité de son site Web.**

