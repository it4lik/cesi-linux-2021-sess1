# TP2 : Serveur Web

Ce deuxième TP a pour but d'approfondir la gestion de services. 

Au menu :
* serveur web qui nécessite une base de données
* une base de données
* un reverse proxy qui permet d'augmenter le niveau de sécurité global

Pour l'application Web, on va installer un bête Wordpress. C'est un outil qu'on voit un peu partout et qui représente un bon cas d'école dans notre cas.

# Sommaire

<!-- vim-markdown-toc GitLab -->

* [0. Prérequis](#0-prérequis)
    * [SELinux](#selinux)
    * [Réseau](#réseau)
* [I. Base de données](#i-base-de-données)
* [II. Serveur Web](#ii-serveur-web)
* [III. Reverse Proxy](#iii-reverse-proxy)
* [IV. Un peu de sécu](#iv-un-peu-de-sécu)
    * [1. fail2ban](#1-fail2ban)
    * [2. HTTPS](#2-https)
        * [Présentation](#présentation)
        * [A. Génération du certificat et de la clé privée associée](#a-génération-du-certificat-et-de-la-clé-privée-associée)
        * [B. Configurer le reverse proxy](#b-configurer-le-reverse-proxy)
    * [3. Monitoring](#3-monitoring)
        * [Présentation](#présentation-1)
        * [A. Installation de Netdata](#a-installation-de-netdata)
        * [B. Alerting](#b-alerting)
        * [C. Bonus](#c-bonus)
    * [4. Interface Web](#4-interface-web)

<!-- vim-markdown-toc -->

# 0. Prérequis

> **Cette partie Prérequis n'a pas besoin de figurer dans le compte-rendu.**

Chaque service (serveur web, base de données et reverse proxy) devront être installés sur une machine dédiée. Trois machines virtuelles à créer donc (cloner la machine que vous avez déjà trois fois).

## SELinux

SELinux doit toujours être désactivé sur toutes les machines :
```bash
$ sudo setenforce 0

$ sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
```

## Réseau 

N'oubliez pas de changer la configuration réseau après le clonage.

Faites en sorte que les machines utilisent des noms de domaine :
* modification de leur fichier `/etc/hosts`
* modification de leur fichier `/etc/hostname`
* les machines seront nommées `web.tp2.cesi`, `db.tp2.cesi` et `rp.tp2.cesi`

Tableau d'adressage (ce sont les adresses host-only, vous pouvez créer un nouveau réseau host-only) :

| Machine        | Adresse IP    |
|----------------|---------------|
| `web.tp2.cesi` | `10.99.99.11` |
| `db.tp2.cesi`  | `10.99.99.12` |
| `rp.tp2.cesi`  | `10.99.99.13` |

# I. Base de données

> A faire uniquement sur `db.tp2.cesi`.

Installer le paquet `mariadb-server`, puis démarrer le service associé.

Il faudra créer un utilisateur dans l'instance de base de données, ainsi qu'une base dédiée. Référez-vous à des docs/articles que vous pourrez trouver sur internet concernant la préparation d'une base de données pour une utilisation avec Wordpress. Les étapes :
* se connecter à la base
* créer une base
* créer un utilisateur
* attribuer les droits sur la base de données à l'utilisateur 
* ouvrir un port firewall pour que d'autres machines puissent accéder à la base

Pour connaître le port utilisé par la base de données, on peut uiliser la commande `ss`. Une fois la base de données lancée, on peut par exemple la commande suivante pour déterminer le port utilisé par la base :
```bash
$ sudo ss -alnpt
```

Pour tester votre base de données :
```bash
$ mysql -u <USER> -p -h <IP_BASE_DE_DONNEES> -P <PORT_BASE_DE_DONNEES>

# Exemple de commande SQL à taper pour vérifier que la base fonctionne
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
+--------------------+
2 rows in set (0.00 sec)
```

# II. Serveur Web

> A faire uniquement `web.tp2.cesi`

Installer un serveur Apache (paquet `httpd` dans CentOS).

Télécharger Wordpress : https://wordpress.org/latest.tar.gz

Idem ici, référez-vous à un des milliers d'article/doc/tuto que vous pourrez trouver. Faites vos recherches en anglais et précisez l'OS qu'on utilise.

Les étapes :
* installer le serveur Web (`httpd`) et le langage PHP (pour installer PHP, voir les instructions plus bas)
* télécharger wordpress
* extraire l'archive wordpress dans un dossier qui pourra être servi par Apache
* renseigner à Wordpress les informations qu'il utilisera pour se connecter à la base de données (dans son fichier de configuration)
* configurer Apache pour qu'il serve le dossier où se trouve Wordpress
* lancer le serveur de base de donnée, puis le serveur web
* ouvrir le port firewall sur le serveur web
* accéder à l'interface de Wordpress afin de valider la bonne installation de la solution

Installation de PHP : la version de PHP disponible dans les dépôts officiels de CentOS7 n'est pas compatible avec la dernière version de Wrodpress disponible. Pour installer un version de PHP compatible :
```bash
# Installation de dépôt additionels
$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
$ sudo yum install -y yum-utils

# On supprime d'éventuelles vieilles versions de PHP précédemment installées
$ sudo yum remove -y php

# Activation du nouveau dépôt
$ sudo yum-config-manager --enable remi-php56  

# Installation de PHP 5.6.40 et de librairies récurrentes
$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
```

# III. Reverse Proxy

> A faire uniquement `rp.tp2.cesi`.

Installer un serveur NGINX (paquet `epel-release` puis `nginx` sur CentOS).

Configurer NGINX pour qu'il puisse renvoyer vers le serveur web lorsqu'on l'interroge sur le port 80. Une configuration du fichier `/etc/nginx/nginx.conf` extrêmement minimale peut ressembler à :
```
events {}

http {
    server {
        listen       80;

        server_name web.tp2.cesi;

        location / {
            proxy_pass   http://<IP:PORT>;
        }
    }
}
```

> Il sera nécessaire de remplacer la mention `<IP:PORT>` par l'IP et le port où est joignable le serveur Web.

Une fois cela effectué, configurer le firewall de NGINX afin d'accepter ce trafic. 

Tester qu'on accède bien au Wordpress en passant par l'IP du reverse proxy. 

> Pour avoir quelque chose de clean et similaire à un monde réel (mais sans DNS) vous pouvez ajouter au fichier `/etc/hosts` (ou équivalent) du **CLIENT** l'adresse IP du reverse proxy qui pointe vers le nom "web.tp2.cesi". Ainsi le client accède à l'application sur le nom `http://web.tp2.cesi`.

# IV. Un peu de sécu

La "sécurité" au sens large fait référence à tout ce qui nous permet d'augmenter le niveau de disponibilité, d'intégrité ou de confidentialité de nos données ou applications. 

Les points abordés dans cette section : 
* protection flood avec fail2ban
  * protection d'un spam/bruteforce de l'accès SSH
* gestion de certificats pour mettre en place une interface web en HTTPS
  * certificat auto-signé
  * c'est le reverse proxy qui se chargera d'effectuer le chiffrement
* monitoring avec Netdata
  * solution simple de mise en oeuvre et très puissante

## 1. fail2ban

> A faire sur toutes les machines où on se connecte avec SSH.

fail2ban est un outil très commun au sein des systèmes GNU/Linux, il permet de limiter les attaques à base de flood/spam (comme attaque par bruteforce).

Son fonctionnement est très simple :
* il lit en temps réel des fichiers de logs
* il repère dans ces fichiers de logs des éléments importants (expression régulière)
* il effectue des actions en fonction de ce qu'il a vu dans les logs

Cas que vous devez mettre en place :
* il lit en temps réel le fichier de log du démon SSH `sshd`
* `sshd` écrit chaque tentative de connexion dans le fichier
* si un certain nombre d'échecs de connexions sont réalisés dans un certain laps de temps, fail2ban devra le détecter
* fail2ban utilisera alors le pare-feu pour bloquer l'IP qui a essayé de se connecter

Là encore, inspirez-vous de ce que vous pourrez trouver sur Internet, c'est un cas d'école très classique.

## 2. HTTPS

### Présentation

La mise en place du protocole TLS (anciennement SSL) pour servir nos sites web permet d'augmenter le niveau de sécurité global du site. 

L'utilisation d'un tel chiffrement garantit au client un chiffrement complet de toutes les données qu'il envoie au site : on garantit donc la confidentialité des données pendant leur transit sur le réseau. 

Il est aussi possible de demander à une autorité de certification (une "CA") connue et de confiance de signer notre certificat. Ainsi, les clients auront la garantie de notre identité en tant que service Web.

Dans notre TP, nous signerons nous-même notre certificat pour éviter d'avoir à s'encombrer d'une signature payante ou d'un nom de domaine (souvent nécessaire pour les signatures par des CA gratuites).

Pour que l'utilisation de HTTP fonctionne il est **obligatoire** que les clients utilisent un nom d'hôte et de domaine pour accéder à l'application (PAS une adresse IP). Sans DNS, ils nous faudra alors dans ce TP configurer le fichier `/etc/hosts` des clients.

### A. Génération du certificat et de la clé privée associée

> A faire sur la machine frontale Web : le reverse proxy.

Commande pour générer une clé et un certificat auto-signé :
```bash
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.tp2.cesi.key -out web.tp2.cesi.crt
```

**NB :** lorsque vous taperez la commande de génération de certificat, un prompt vous demandera des infos qui seront inscrites dans le certificat. Vous pouvez mettre ce que vous voulez **SAUF** pour l'étape où on vous demande :
```bash
Common Name (e.g. server FQDN or YOUR name) []:
```
**Il est impératif de saisir ici le FQDN ou hostname qui sera utilisé par les clients pour joindre le service.** Dans notre cas c'est `web.tp2.cesi`.

Le certificat et la clé ont été générés dans le répertoire courant. Vous pouvez les déplacez où vous le souhaitez ; il existe cependant un path réservé à ça dans CentOS :
* `/etc/pki/tls/certs/` pour les certificats
* `/etc/pki/tls/private/` pour les clés

### B. Configurer le reverse proxy

> A faire sur la machine frontale Web : le reverse proxy.

Configurer le reverse proxy pour écouter sur le port 443 (TCP). Le port 443 doit mettre en place un chiffrement TLS avec la clé et le certificat qui viennent d'être générés. Il doit toujours rediriger vers l'application Web Wordpress.

Si le port 80 est atteint, le trafic doit être redirigé automatiquement vers le port 443 (ça se fait dans la configuration de NGINX).

> Gardez en tête la commande `sudo ss -alnpt` pour avoir une idée des ports TCP en écoute sur votre machine. Vous pouvez vous assurer que votre serveur tourne bien sur le port `80/tcp` et `443/tcp` de cette façon. Pour tester la redirection automatique de port 80 vers 443, il faudra vous connecter au site comme un client normal, avec un navigateur.

N'oubliez pas d'ajouter au fichier `hosts` du CLIENT que `web.tp2.cesi` doit pointer vers l'adresse IP du reverse proxy. Si vous testez avec votre machine hôte et votre navigateur, c'est au fichier `hosts` de VOTRE PC qu'il faut ajouter l'IP et le nom `web.tp2.cesi`.

## 3. Monitoring

### Présentation

Nous allons dans cette section mettre en place un outil de monitoring et d'alerting simple mais puissant : Netdata. 

Suivez la [doc officielle](https://learn.netdata.cloud/docs/get) pour l'installer. L'interface de l'outil est alors disponible sur le port `19999` du serveur (interface Web).

Netdata a pour avantages de ne consommer que très peu de ressourcesn mais néanmoins remonter une grande quantité de métriques, afin de les exploiter pour du monitoring ou la mise en place d'alertes.

### A. Installation de Netdata

Installer Netdata sur les 3 machines et tester qu'il fonctionne bien en accédant à l'interface Web. Il faudra ouvrir le port firewall `19999/tcp` pour accéder à l'interface de Netdata.

### B. Alerting

Définir une configuration qui permet de recevoir une [alerte Discord](https://learn.netdata.cloud/docs/agent/health/notifications/discord) si le disque OU la RAM sont remplis à + de 75%.

### C. Bonus

En bonus : ajouter une configuration au Reverse Proxy pour qu'il permette d'accéder aux trois interfaces de Netdata, avec du chiffrement.

On peut aussi affiner la configuration réseau du parc pour empêcher que l'application Web ou les interfaces de Netdata soient disponibles sans passer par le reverse proxy. Plusieurs façons de faire, par ex :

**Faire ça niveau 1**
* on crée un non pas un, mais deux réseaux locaux
* l'un est utilisé pour la communication entre nos serveurs + l'administration *via* SSH
* un deuxième est utilisé entre les clients et les serveurs frontaux (en l'occurrence, juste notre reverse proxy)

> Ca arrive même d'en voir un troisième réservé à l'admin SSH exclusivement, un autre pour les flux de backup, etc.

**Faire ça niveau 3**
* configurer le pare-feu des serveurs Web et base de données pour qu'ils refusent toutes connexions non attendues
* on blacklist tout
* le serveur web whitelist la base de données et le reverse proxy
* la base de de donnée whitelist que le serveur Web
* n'oubliez pas qu'il y a aussi votre accès SSH

Liste non-exhautive des solutions. Plus c'est fait tôt (niveau 1), plus c'est radical, et moins la surface d'attaque est grande, mais plus c'est coûteux à mettre en oeuvre. Plus c'est fait tard (niveau 3) plus c'est compliqué à maintenir dans le temps (en l'occurrence : il faut les maintenir les règles firewall).

On peut aussi cumuler les deux mesures sans problèmes pour minimiser la surface d'attaque de nos serveurs.

## 4. Interface Web

Un outil qu'il est possible d'utiliser pour contrôler certains aspects du serveur est Cockpit.

Son installation est très simple :
```bash
$ sudo yum install -y epel-release
$ sudo yum install -y cockpit
```

Il sera nécessaire d'ouvrir le port 9090/tcp.

Il faut ensuite démarrer le service `cockpit`. 

Le service est alors accessible avec un navigateur `http://10.88.88.10:9090`

> Il est aussi possible d'ajouter une vraie interface graphique de bureau au serveur, vous trouverez [beaucoup d'articles sur internet](https://geekflare.com/centos-gui-mode/).
